# Iron Age people

This repository is intended to contain analyses of the project: *Demography-based transformations in Iron Age in Europe* and the collective work of the subproject A2, E4 and F6 of the [CRC1266: Scales of Transformation](https://www.sfb1266.uni-kiel.de/en).
Maintainers: Lizzie Scholtus, Carole Quatrelivre, Camilla Zaviani, Giacomo Bilotti

## The Data

### Test Data

The first dataset to be tested for demography purposes was extracted from the French database BaseFer: https://www.chronocarto.eu/spip.php?article27&lang=en . Coordinates are WGS84.


## The Analysis

